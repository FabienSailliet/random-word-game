class WordPicker {
    constructor(sourceURL, bufferSize) {
        this.sourceURL = sourceURL;
        this.bufferSize = bufferSize;

        this._buffer = [];
        this._fillBuffer();
        this._waiting = undefined;
    }

    pickWord(success) {
        if (this._buffer.length == 0) {
            this._waiting = success;
        }
        
        // If buffer half-empty, ask for new words
        if (this._buffer.length <= this.bufferSize/2) {
            this._fillBuffer();
        }

        success(this._buffer.pop());
    }

    _addWords(words) {
        this._buffer = this._buffer.concat(words);

        if (this._waiting != undefined) {
            this._waiting(this._buffer.pop());
            this._waiting = undefined;
        }
    }
    
    _fillBuffer() {
        let wordPicker = this;

        $.getJSON(
            this.sourceURL,
            {
                'nb': this.bufferSize - this._buffer.length
            },
            function (words) {
                wordPicker._addWords(words);
            }
        );
    }
}

function pickANewWord() {
    wordPicker.pickWord(setWord);
}

function setWord(word) {
    $('#word').text(word);
}

function definitionButtonClicked() {
    let word = $('#word').text();

    let win = window.open(
        'https://fr.wiktionary.org/wiki/' + word,
        'definition_tab'
    );

    if (win) {
        win.focus();
    }
}

var wordPicker = new WordPicker('randomwords.php', 100);

$(document).ready(function () {
    $('#newwordbutton').click(pickANewWord);
    $('#definitionbutton').click(definitionButtonClicked);
});