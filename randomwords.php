<?php

define(DIC_FILE, 'dico.txt');

$wordsNb = 0;
if (isset($_GET['nb'])) {
    $wordsNb = $_GET['nb'];
}

$lines = file(DIC_FILE, FILE_IGNORE_NEW_LINES);

$ret = [];
for ($i=0 ; $i<$wordsNb ; $i++) {
    $ret[] = $lines[array_rand($lines)];
}

echo json_encode($ret);

?>